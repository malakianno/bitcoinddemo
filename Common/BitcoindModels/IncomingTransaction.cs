﻿using System.Collections.Generic;

namespace Common.BitcoindModels
{
    public class IncomingTransaction
    {
        public string Address { get; set; }
        public string Account { get; set; }
        public decimal Amount { get; set; }
        /// <summary>
        /// Number of confirmations of the most recent transaction included
        /// </summary>
        public uint Confirmations { get; set; }
        public string Label { get; set; }
        public IEnumerable<string> TxIds { get; set; }
    }
}