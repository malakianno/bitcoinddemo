﻿using System.Collections.Generic;

namespace Common.BitcoindModels
{
    public class Transaction
    {
        public string TxId { get; set; }
        public decimal Amount { get; set; }
        public decimal Fee { get; set; }
        public uint Confirmations { get; set; }
        public string BlockHash { get; set; }
        public uint BlockTime { get; set; }
        public bool Trusted { get; set; }
        public uint Time { get; set; }
        public uint TimeReceived { get; set; }
        public string Hex { get; set; }
        public IEnumerable<TransactionDetails> Details { get; set; }
    }
}