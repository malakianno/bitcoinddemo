﻿using Common.Enums;

namespace Common.BitcoindModels
{
    public class TransactionDetails
    {
        public string Account { get; set; }
        public string Address { get; set; }
        public TransactionCategoryEnum Category { get; set; }
        public decimal Amount { get; set; }
        public uint Vout { get; set; }
        public decimal Fee { get; set; }
        public bool Abandoned { get; set; }
    }
}