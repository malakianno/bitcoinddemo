﻿using Common.Enums;

namespace Common.BitcoindModels
{
    public class TransactionListItem
    {
        public string TxId { get; set; }
        public string Account { get; set; }
        public string Address { get; set; }
        public TransactionCategoryEnum Category { get; set; }
        public decimal Amount { get; set; }
        public string Label { get; set; }
        public uint Vout { get; set; }
        public uint Confirmations { get; set; }
        public string BlockHash { get; set; }
        public uint BlockIndex { get; set; }
        public ulong BlockTime { get; set; }
        public ulong Time { get; set; }
        public ulong TimeReceived { get; set; }
    }
}