﻿namespace Common.Enums
{
    public enum TransactionCategoryEnum
    {
        Move,
        Receive,
        Send
    }
}