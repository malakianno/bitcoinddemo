﻿using System;
using System.Linq;
using Common.Models;
using Common.Utils;

namespace Common.Mappers
{
    public class TransactionMapper
    {
        public Transaction Map(BitcoindModels.Transaction transaction)
        {
            var details = transaction.Details.FirstOrDefault();
            return new Transaction()
            {
                TxId = transaction.TxId,
                Amount = Math.Abs(transaction.Amount),
                Fee = Math.Abs(transaction.Fee),
                Confirmations = transaction.Confirmations,
                Time = TimeUtil.FromEpoch(transaction.Time),
                Address = details?.Address ?? string.Empty
            };
        }
    }
}
