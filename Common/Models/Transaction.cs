﻿using System;

namespace Common.Models
{
    public class Transaction
    {
        public string TxId { get; set; }
        public decimal Amount { get; set; }
        public decimal? Fee { get; set; }
        public uint Confirmations { get; set; }
        public DateTime Time { get; set; }
        public string Address { get; set; }
    }
}
