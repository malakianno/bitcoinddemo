﻿using System;
using System.Collections.Generic;

namespace Common.Models
{
    public class Account
    {
        public Guid? UID { get; set; }
        public string Name { get; set; }
        public decimal Balance { get; set; }
        public IEnumerable<string> Addresses { get; set; }
    }
}
