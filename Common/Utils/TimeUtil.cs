﻿using System;

namespace Common.Utils
{
    public static class TimeUtil
    {
        private static readonly DateTime EpochDateTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        public static DateTime FromEpoch(ulong seconds)
        {
            return EpochDateTime.AddSeconds(seconds);
        }
    }
}