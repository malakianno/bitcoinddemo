USE [bdb]
GO

DROP TABLE [dbo].[IncomingTransactions]
GO
DROP TABLE [dbo].[OutgoingTransactions]
GO
DROP TABLE [dbo].[AccountsAddresses]
GO
DROP TABLE [dbo].[Accounts]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

--------------------------------------- [Accounts] --------------------------------------------------------
CREATE TABLE [dbo].[Accounts](
	[UID] [uniqueidentifier] NOT NULL,
	[AccountName] [varchar](900) NOT NULL,
	[Balance] [decimal](18, 8) NOT NULL,
 CONSTRAINT [PK_Accounts] PRIMARY KEY CLUSTERED 
(
	[UID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Accounts] ON [dbo].[Accounts]
(
	[AccountName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
--------------------------------------- [AccountsAddresses] --------------------------------------------------------
CREATE TABLE [dbo].[AccountsAddresses](
	[AccountUID] [uniqueidentifier] NOT NULL,
	[Address] [varchar](35) NOT NULL
)
GO
ALTER TABLE [dbo].[AccountsAddresses]  WITH CHECK ADD  CONSTRAINT [FK_AccountsAddresses_Accounts] FOREIGN KEY([AccountUID])
REFERENCES [dbo].[Accounts] ([UID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_AccountsAddresses] ON [dbo].[AccountsAddresses]
(
	[Address] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
--------------------------------------- [OutgoingTransactions] --------------------------------------------------------
CREATE TABLE [dbo].[OutgoingTransactions](
	[TransactionId] [varchar](256) NOT NULL,
	[AccountUID] [uniqueidentifier] NULL,
	[Address] [varchar](35) NOT NULL,
	[Amount] [decimal](18, 8) NOT NULL,
	[Fee] [decimal](18, 8) NULL,
	[Time] [datetime] NOT NULL,
 CONSTRAINT [PK_OutgoingTransactions] PRIMARY KEY CLUSTERED 
(
	[TransactionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[OutgoingTransactions]  WITH CHECK ADD  CONSTRAINT [FK_OutgoingTransactions_Accounts] FOREIGN KEY([AccountUID])
REFERENCES [dbo].[Accounts] ([UID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
--------------------------------------- [IncomingTransactions] --------------------------------------------------------
CREATE TABLE [dbo].[IncomingTransactions](
	[TransactionId] [varchar](256) NOT NULL,
	[Address] [varchar](35) NOT NULL,
	[Amount] [decimal](18, 8) NOT NULL,
	[Confirmations] [int] NOT NULL,
	[Time] [datetime] NOT NULL,
	[IsShown] [bit] default 0 NOT NULL,
 CONSTRAINT [PK_IncomingTransactions] PRIMARY KEY CLUSTERED 
(
	[TransactionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_IncomingTransactions] ON [dbo].[IncomingTransactions]
(
	[Confirmations] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO