﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Common.Models;

namespace DAL.Repositories
{
    public class AccountsRepository : BaseRepository
    {
        public async Task UpdateAccounts(IEnumerable<Account> accounts)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                await connection.OpenAsync();
                using (var transaction = connection.BeginTransaction())
                {
                    try
                    {
                        foreach (var account in accounts)
                        {
                            await SynhronizeAccount(account, connection, transaction);
                        }
                        var accountsParamsNames = new List<string>();
                        using (var cmd = new SqlCommand())
                        {
                            var index = 0;
                            foreach (var account in accounts)
                            {
                                var paramName = $"@Account{index++}";
                                accountsParamsNames.Add(paramName);
                                cmd.Parameters.AddWithValue(paramName, account.Name);
                            }
                            cmd.Connection = connection;
                            cmd.CommandText = $"DELETE FROM dbo.Accounts WHERE AccountName NOT IN ({string.Join(", ", accountsParamsNames)})";
                            cmd.Transaction = transaction;
                            await cmd.ExecuteNonQueryAsync();
                        }
                        transaction.Commit();
                    }
                    catch (Exception e)
                    {
                        transaction.Rollback();
                        throw e;
                    }
                }
            }
        }

        private async Task SynhronizeAccount(Account account, SqlConnection connection, SqlTransaction transaction)
        {
            using (var cmd = new SqlCommand(AddOrUpdateAccountQuery, connection, transaction))
            {
                cmd.Parameters.Add("@AccountName", SqlDbType.VarChar, 900).Value = account.Name;
                cmd.Parameters.Add("@Balance", SqlDbType.Decimal).Value = account.Balance;
                var result = await cmd.ExecuteScalarAsync();
                if (!account.UID.HasValue)
                {
                    var accountUid = Guid.Parse(result.ToString());
                    account.UID = accountUid;
                }
            }
            var addressesParamsNames = new List<string>();
            using (var cmd = new SqlCommand())
            {
                var index = 0;
                foreach (var address in account.Addresses)
                {
                    var paramName = $"@Address{index++}";
                    addressesParamsNames.Add(paramName);
                    cmd.Parameters.AddWithValue(paramName, address);
                }
                cmd.Parameters.Add("@AccountUID", SqlDbType.UniqueIdentifier).Value = account.UID;
                cmd.Connection = connection;
                cmd.CommandText = $"DELETE FROM dbo.AccountsAddresses WHERE AccountUID = @AccountUID AND Address NOT IN ({string.Join(", ", addressesParamsNames)})";
                cmd.Transaction = transaction;
                await cmd.ExecuteNonQueryAsync();
            }
            foreach (var address in account.Addresses)
            {
                using (var cmd = new SqlCommand(AddAccountAddressQuery, connection, transaction))
                {
                    cmd.Parameters.Add("@AccountUID", SqlDbType.UniqueIdentifier).Value = account.UID;
                    cmd.Parameters.Add("@Address", SqlDbType.VarChar, 35).Value = address;
                    await cmd.ExecuteNonQueryAsync();
                }
            }
        }

        public async Task UpdateAccountsBalances(IDictionary<string, decimal> accounts)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                await connection.OpenAsync();
                foreach (var account in accounts)
                {
                    using (var cmd = new SqlCommand(UpdateAccountBalanceQuery, connection))
                    {
                        cmd.Parameters.Add("@AccountName", SqlDbType.VarChar, 900).Value = account.Key;
                        cmd.Parameters.Add("@Balance", SqlDbType.Decimal).Value = account.Value;
                        await cmd.ExecuteNonQueryAsync();
                    }
                }
            }
        }

        public async Task<(Guid?, string)> GetAccountWithMaxBalance()
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                await connection.OpenAsync();
                using (var cmd = new SqlCommand(GeAccountWithMaxBalanceQuery, connection))
                {
                    using (var reader = await cmd.ExecuteReaderAsync())
                    {
                        if (reader.Read())
                        {
                            var accountUID = Guid.Parse(reader["UID"].ToString());
                            var accountName = reader["AccountName"].ToString();
                            return (accountUID, accountName);
                        }
                        return (null, null);
                    }
                }
            }
        }

        public async Task<IEnumerable<string>> GetAccountsNames()
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                await connection.OpenAsync();
                using (var cmd = new SqlCommand("SELECT AccountName FROM dbo.Accounts", connection))
                {
                    using (var reader = await cmd.ExecuteReaderAsync())
                    {
                        var accounts = new List<string>();
                        while (reader.Read())
                        {
                            var accountName = reader["AccountName"].ToString();
                            accounts.Add(accountName);
                        }
                        return accounts;
                    }
                }
            }
        }

        private const string AddOrUpdateAccountQuery = @"
            IF EXISTS(SELECT 1 FROM dbo.Accounts WHERE AccountName = @AccountName)
            BEGIN
	            UPDATE dbo.Accounts SET Balance = @Balance WHERE AccountName = @AccountName
            END
            ELSE
            BEGIN
	            INSERT INTO dbo.Accounts VALUES (NEWID(), @AccountName, @Balance)
            END
            SELECT UID FROM dbo.Accounts WHERE AccountName = @AccountName
        ";

        private const string AddAccountAddressQuery = @"
            IF NOT EXISTS(SELECT 1 FROM dbo.AccountsAddresses WHERE AccountUID = @AccountUID AND Address = @Address)
            BEGIN
	            INSERT INTO dbo.AccountsAddresses VALUES (@AccountUID, @Address)
            END
        ";

        private const string UpdateAccountBalanceQuery = "UPDATE dbo.Accounts SET Balance = @Balance WHERE AccountName = @AccountName";

        private const string GeAccountWithMaxBalanceQuery = @"
            SELECT TOP 1
	            UID
	            ,AccountName
            FROM
	            dbo.Accounts
            WHERE
	            Balance = (SELECT MAX(Balance) FROM dbo.Accounts)
        ";
    }
}
