﻿using System.Configuration;

namespace DAL.Repositories
{
    public class BaseRepository
    {
        protected static readonly string _connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
    }
}
