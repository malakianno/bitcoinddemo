﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Common.BitcoindModels;
using Common.Utils;
using Transaction = Common.Models.Transaction;

namespace DAL.Repositories
{
    public class TransactionsRepository : BaseRepository
    {
        public async Task UpdateIncomingTransactions(IEnumerable<TransactionListItem> txsToAdd, IEnumerable<TransactionListItem> txsToUpdate, IEnumerable<string> txsIdsToRemove)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                await connection.OpenAsync();
                using (var sqlTransaction = connection.BeginTransaction())
                {
                    try
                    {
                        foreach (var transaction in txsToAdd)
                        {
                            using (var cmd = new SqlCommand(AddIncomingTransactionQuery, connection, sqlTransaction))
                            {
                                cmd.Parameters.Add("@TxId", SqlDbType.VarChar).Value = transaction.TxId;
                                cmd.Parameters.Add(@"Address", SqlDbType.VarChar).Value = transaction.Address;
                                cmd.Parameters.Add(@"Amount", SqlDbType.Decimal).Value = transaction.Amount;
                                cmd.Parameters.Add(@"Confirmations", SqlDbType.Int).Value = transaction.Confirmations;
                                cmd.Parameters.Add(@"IsShown", SqlDbType.Bit).Value = false;
                                cmd.Parameters.Add(@"Time", SqlDbType.DateTime).Value = TimeUtil.FromEpoch(transaction.TimeReceived);
                                await cmd.ExecuteNonQueryAsync();
                            }
                        }
                        foreach (var transaction in txsToUpdate)
                        {
                            using (var cmd = new SqlCommand(UpdateIncomingTransactionQuery, connection, sqlTransaction))
                            {
                                cmd.Parameters.Add("@TxId", SqlDbType.VarChar).Value = transaction.TxId;
                                cmd.Parameters.Add(@"Confirmations", SqlDbType.Int).Value = transaction.Confirmations;
                                await cmd.ExecuteNonQueryAsync();
                            }
                        }
                        foreach (var txId in txsIdsToRemove)
                        {
                            using (var cmd = new SqlCommand(RemoveIncomingTransactionQuery, connection, sqlTransaction))
                            {
                                cmd.Parameters.Add("@TxId", SqlDbType.VarChar).Value = txId;
                                await cmd.ExecuteNonQueryAsync();
                            }
                        }
                        sqlTransaction.Commit();
                    }
                    catch (Exception e)
                    {
                        sqlTransaction.Rollback();
                        throw e;
                    }
                }
            }
        }

        public async Task UpdateOutgoingTransactions(IEnumerable<TransactionListItem> txsToAdd, IEnumerable<string> txsIdsToRemove)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                await connection.OpenAsync();
                using (var sqlTransaction = connection.BeginTransaction())
                {
                    try
                    {
                        foreach (var tx in txsToAdd)
                        {
                            using (var cmd = new SqlCommand(AddHistoricalOutgoingTransactionQuery, connection, sqlTransaction))
                            {
                                cmd.Parameters.Add("@TxId", SqlDbType.VarChar).Value = tx.TxId;
                                cmd.Parameters.Add(@"Address", SqlDbType.VarChar).Value = tx.Address;
                                cmd.Parameters.Add(@"Amount", SqlDbType.Decimal).Value = tx.Amount;
                                cmd.Parameters.Add(@"Time", SqlDbType.DateTime).Value = TimeUtil.FromEpoch(tx.Time);
                                await cmd.ExecuteNonQueryAsync();
                            }
                        }
                        foreach (var txId in txsIdsToRemove)
                        {
                            using (var cmd = new SqlCommand(RemoveIncomingTransactionQuery, connection, sqlTransaction))
                            {
                                cmd.Parameters.Add("@TxId", SqlDbType.VarChar).Value = txId;
                                await cmd.ExecuteNonQueryAsync();
                            }
                        }
                        sqlTransaction.Commit();
                    }
                    catch (Exception e)
                    {
                        sqlTransaction.Rollback();
                        throw e;
                    }
                }
            }
        }

        public async Task AddOutgoingTransaction(Transaction transaction, Guid accountUID)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                await connection.OpenAsync();
                using (var cmd = new SqlCommand(AddOutgoingTransactionQuery, connection))
                {
                    cmd.Parameters.Add("@TxId", SqlDbType.VarChar).Value = transaction.TxId;
                    cmd.Parameters.Add(@"AccountUID", SqlDbType.UniqueIdentifier).Value = accountUID;
                    cmd.Parameters.Add(@"Address", SqlDbType.VarChar).Value = transaction.Address;
                    cmd.Parameters.Add(@"Amount", SqlDbType.Decimal).Value = transaction.Amount;
                    cmd.Parameters.Add(@"Fee", SqlDbType.Decimal).Value = transaction.Fee;
                    cmd.Parameters.Add(@"Time", SqlDbType.DateTime).Value = transaction.Time;
                    await cmd.ExecuteNonQueryAsync();
                }
            }
        }

        public async Task<IEnumerable<string>> GetIncomingTransactions()
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                await connection.OpenAsync();
                return await GetTransactionsIds(connection, GetAllIncomingTxsQuery);
            }
        }

        public async Task<IEnumerable<string>> GetOutgoingTransactions()
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                await connection.OpenAsync();
                return await GetTransactionsIds(connection, GetAllOutgoingTxsQuery);
            }
        }

        public async Task<IEnumerable<string>> GetObservableIncomingTransactions()
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                await connection.OpenAsync();
                return await GetTransactionsIds(connection, GetObservableIncomingTxsQuery);
            }
        }

        public async Task<IEnumerable<Transaction>> GetLatestIncomingTransactions()
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                await connection.OpenAsync();
                using (var cmd = new SqlCommand(GetLatestIncomingTransactionsQuery, connection))
                {
                    using (var reader = await cmd.ExecuteReaderAsync())
                    {
                        var result = new List<Transaction>();
                        while (reader.Read())
                        {
                            var transaction = new Transaction()
                            {
                                TxId = reader["TransactionId"].ToString(),
                                Address = reader["Address"].ToString(),
                                Amount = Convert.ToDecimal(reader["Amount"]),
                                Confirmations = Convert.ToUInt32(reader["Confirmations"]),
                                Time = Convert.ToDateTime(reader["Time"]),
                            };
                            result.Add(transaction);
                        }
                        return result;
                    }
                }
            }
        }

        private async Task<IEnumerable<string>> GetTransactionsIds(SqlConnection connection, string query)
        {
            using (var cmd = new SqlCommand(query, connection))
            {
                using (var reader = await cmd.ExecuteReaderAsync())
                {
                    var txIds = new List<string>();
                    while (reader.Read())
                    {
                        var txId = reader["TransactionId"].ToString();
                        txIds.Add(txId);
                    }
                    return txIds;
                }
            }
        }

        private const string GetObservableIncomingTxsQuery = "SELECT TransactionId FROM dbo.IncomingTransactions WHERE Confirmations <= 6";

        private const string GetAllIncomingTxsQuery = "SELECT TransactionId FROM dbo.IncomingTransactions";

        private const string GetAllOutgoingTxsQuery = "SELECT TransactionId FROM dbo.OutgoingTransactions";

        private const string AddIncomingTransactionQuery = "INSERT INTO dbo.IncomingTransactions VALUES (@TxId, @Address, @Amount, @Confirmations, @Time, @IsShown)";

        private const string UpdateIncomingTransactionQuery = @"
            UPDATE
                dbo.IncomingTransactions
            SET
                Confirmations = @Confirmations
            WHERE
                TransactionId = @TxId";

        private const string RemoveIncomingTransactionQuery = @"DELETE FROM dbo.IncomingTransactions WHERE TransactionId = @TxId";

        private const string AddOutgoingTransactionQuery = @"
            INSERT INTO
                dbo.OutgoingTransactions
            VALUES
            (
                @TxId
                ,@AccountUID
                ,@Address
                ,@Amount
                ,@Fee
                ,@Time
            )
        ";

        private const string AddHistoricalOutgoingTransactionQuery = @"
            INSERT INTO
                dbo.OutgoingTransactions
            VALUES
            (
                @TxId
                ,NULL
                ,@Address
                ,@Amount
                ,NULL
                ,@Time
            )
        ";

        private const string GetLatestIncomingTransactionsQuery = @"
            UPDATE
                dbo.IncomingTransactions
            SET
                IsShown = 1
            OUTPUT
                INSERTED.TransactionId
	            ,INSERTED.Address
	            ,INSERTED.Amount
	            ,INSERTED.Confirmations
	            ,INSERTED.Time
            WHERE
	            Confirmations < 3
	            OR IsShown = 0
        ";
    }
}
