﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DAL.Repositories;
using WebApp.Services;
using WebApp.Models;

namespace Tests
{
    [TestClass]
    public class Tests
    {
        [TestInitialize]
        public async Task Init()
        {
            var syncService = new SynchronizationService();
            await syncService.SynchronizeAccounts();
            await syncService.SynchronizeTransactions();
        }

        [TestMethod]
        public async Task SendBtc()
        {
            const decimal amount = 0.001m;
            const string address = "mrXmR1aoDu9J18A6ZRV1jcZ42F3ZNArPtF";
            try
            {
                var transactionsRepository = new TransactionsRepository();
                var walletsRepository = new AccountsRepository();
                var syncService = new SynchronizationService();
                var bitcoindProxyService = new BitcoindProxyService();
                string transactionId = null;
                Guid? accountId = null;
                try
                {
                    await SynchronizationService.SyncSemaphore.WaitAsync();
                    (Guid? accountUID, string accountName) = await walletsRepository.GetAccountWithMaxBalance();
                    if (!string.IsNullOrEmpty(accountName))
                    {
                        accountId = accountUID;
                        transactionId = await bitcoindProxyService.SendBtc(accountName, address, amount);
                        await syncService.UpdateAccountsBalances();
                    }
                }
                finally
                {
                    SynchronizationService.SyncSemaphore.Release();
                }
                if (!string.IsNullOrEmpty(transactionId))
                {
                    var transaction = await bitcoindProxyService.GetTransaction(transactionId);
                    await transactionsRepository.AddOutgoingTransaction(transaction, accountId.Value);
                }
            }
            catch (Exception e)
            {
            }
        }

        [TestMethod]
        public async Task GetLast()
        {
            try
            {
                var transactionsRepository = new TransactionsRepository();
                var latestTransactions = await transactionsRepository.GetLatestIncomingTransactions();
                var result = latestTransactions.Select(tx => new TransactionViewModel()
                {
                    Date = tx.Time,
                    Address = tx.Address,
                    Amount = tx.Amount,
                    Confirmation = tx.Confirmations
                }).OrderByDescending(tx => tx.Date);
            }
            catch (Exception e)
            {
            }
        }
    }
}
