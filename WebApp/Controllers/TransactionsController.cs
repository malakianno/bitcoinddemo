﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using DAL.Repositories;
using WebApp.Models;
using WebApp.Services;

namespace WebApp.Controllers
{
    public class TransactionsController : ApiController
    {
        [HttpPost]
        public async Task SendBtc([FromBody] SendBtcParameters parameters)
        {
            var transactionsRepository = new TransactionsRepository();
            var walletsRepository = new AccountsRepository();
            var syncService = new SynchronizationService();
            var bitcoindProxyService = new BitcoindProxyService();
            string transactionId = null;
            Guid? accountId = null;
            try
            {
                // The following is bottleneck
                await SynchronizationService.SyncSemaphore.WaitAsync();
                (Guid? accountUID, string accountName) = await walletsRepository.GetAccountWithMaxBalance();
                if (!string.IsNullOrEmpty(accountName))
                {
                    accountId = accountUID;
                    transactionId = await bitcoindProxyService.SendBtc(accountName, parameters.Address, parameters.Amount);
                    await syncService.UpdateAccountsBalances();                    
                }
            }
            finally
            {
                SynchronizationService.SyncSemaphore.Release();
            }
            if (!string.IsNullOrEmpty(transactionId))
            {
                var transaction = await bitcoindProxyService.GetTransaction(transactionId);
                await transactionsRepository.AddOutgoingTransaction(transaction, accountId.Value);
            }
        }

        [HttpGet]
        public async Task<IEnumerable<TransactionViewModel>> GetLast()
        {
            var transactionsRepository = new TransactionsRepository();
            var latestTransactions = await transactionsRepository.GetLatestIncomingTransactions();
            return latestTransactions.Select(tx => new TransactionViewModel()
            {
                Date = tx.Time,
                Address = tx.Address,
                Amount = tx.Amount,
                Confirmation = tx.Confirmations
            }).OrderByDescending(tx => tx.Date);
        }
    }
}
