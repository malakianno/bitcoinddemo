﻿namespace WebApp.Enums
{
    enum CommandEnum
    {
        Unknown,
        SendFrom,
        GetAddressesByAccount,
        GetAccountAddress,
        ListAccounts,
        ListReceivedByAddress,
        ListTransactions,
        GetTransaction,
    }
}