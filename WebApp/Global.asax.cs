﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using WebApp.Services;

namespace WebApp
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        private static readonly SynchronizationService _syncService = new SynchronizationService();
        private static readonly Task _syncTask;

        static WebApiApplication()
        {
            _syncTask = new Task(async () => await Sync());
        }

        protected void Application_Start()
        {
            _syncService.SynchronizeAccounts().GetAwaiter().GetResult();
            _syncTask.Start();
            GlobalConfiguration.Configure(WebApiConfig.Register);
        }

        private static async Task Sync()
        {
            while (true)
            {
                try
                {
                    await _syncService.SynchronizeTransactions();
                    await Task.Delay(10);
                }
                catch (Exception)
                {
                }
            }
        }
    }
}
