﻿using System.Collections.Generic;

namespace WebApp.Models.Responses
{
    public class AddressesByAccountResponse : Response
    {
        public IEnumerable<string> Result { get; set; }
    }
}