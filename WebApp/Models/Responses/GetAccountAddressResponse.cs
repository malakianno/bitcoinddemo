﻿namespace WebApp.Models.Responses
{
    public class GetAccountAddressResponse : Response
    {
        public string Result { get; set; }
    }
}