﻿using Common.BitcoindModels;

namespace WebApp.Models.Responses
{
    public class GetTransactionResponse : Response
    {
        public Transaction Result { get; set; }
    }
}