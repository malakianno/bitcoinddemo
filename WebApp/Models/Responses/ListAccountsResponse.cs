﻿using System.Collections.Generic;

namespace WebApp.Models.Responses
{
    public class ListAccountsResponse : Response
    {
        public IDictionary<string, decimal> Result { get; set; }
    }
}