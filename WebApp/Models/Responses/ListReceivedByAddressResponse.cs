﻿using System.Collections.Generic;
using Common.BitcoindModels;

namespace WebApp.Models.Responses
{
    public class ListReceivedByAddressResponse : Response
    {
        public IEnumerable<IncomingTransaction> Result { get; set; }
    }
}