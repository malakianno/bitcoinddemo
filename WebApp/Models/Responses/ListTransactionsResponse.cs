﻿using Common.BitcoindModels;

namespace WebApp.Models.Responses
{
    public class ListTransactionsResponse : Response
    {
        public TransactionListItem[] Result { get; set; }
    }
}