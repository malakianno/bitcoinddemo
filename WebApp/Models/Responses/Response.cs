﻿namespace WebApp.Models.Responses
{
    public class Response
    {
        public string Id { get; set; }
        public string Error { get; set; }
    }
}