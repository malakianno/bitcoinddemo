﻿namespace WebApp.Models.Responses
{
    public class SendToAddressResponse : Response
    {
        /// <summary>
        /// TransactionID
        /// </summary>
        public string Result { get; set; }
    }
}