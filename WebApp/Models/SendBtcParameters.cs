﻿namespace WebApp.Models
{
    public class SendBtcParameters
    {
        public string Address { get; set; }
        public decimal Amount { get; set; }
    }
}