﻿using System;

namespace WebApp.Models
{
    public class TransactionViewModel
    {
        public DateTime Date { get; set; }
        public string Address { get; set; }
        public decimal Amount { get; set; }
        public ulong Confirmation { get; set; }
    }
}