﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Common.BitcoindModels;
using Common.Mappers;
using WebApp.Enums;
using WebApp.Models.Responses;

namespace WebApp.Services
{
    public class BitcoindProxyService
    {
        private static readonly string _bitcoindEndpoint;
        private static readonly NetworkCredential _bitcoindCredential;

        static BitcoindProxyService()
        {
            _bitcoindEndpoint = ConfigurationManager.AppSettings["BitcoindEndpoint"].ToString();
            var userName = ConfigurationManager.AppSettings["UserName"].ToString();
            var password = ConfigurationManager.AppSettings["Password"].ToString();
            _bitcoindCredential = new NetworkCredential(userName, password);
        }

        public async Task<string> SendBtc(string from, string address, decimal amount)
        {
            var parameters = new JArray() { from, address, amount };
            var response = await ExecuteAsync<SendToAddressResponse>(CommandEnum.SendFrom, parameters);
            return response.Result;
        }

        public async Task<IEnumerable<IncomingTransaction>> ListReceivedByAddress(uint minConfirmations, bool includeEmpty)
        {
            var response = await ExecuteAsync<ListReceivedByAddressResponse>(CommandEnum.ListReceivedByAddress, new JArray() { minConfirmations, includeEmpty });
            return response.Result;
        }

        public async Task<TransactionListItem[]> ListTransactions(string account, ulong count, ulong from)
        {
            var parameters = new JArray() { account, count, from };
            var response = await ExecuteAsync<ListTransactionsResponse>(CommandEnum.ListTransactions, parameters);
            foreach(var tx in response.Result)
            {
                tx.Amount = Math.Abs(tx.Amount);
            }
            return response.Result;
        }

        public async Task<Common.Models.Transaction> GetTransaction(string transactionId)
        {
            var parameters = new JArray() { transactionId };
            var response = await ExecuteAsync<GetTransactionResponse>(CommandEnum.GetTransaction, parameters);
            var mapper = new TransactionMapper();
            return mapper.Map(response.Result);
        }

        public async Task<IDictionary<string, decimal>> ListAccounts()
        {
            var response = await ExecuteAsync<ListAccountsResponse>(CommandEnum.ListAccounts, new JArray());
            return response.Result;
        }

        public async Task<string> GetAccountAddress(string account)
        {
            var parameters = new JArray() { account };
            var response = await ExecuteAsync<GetAccountAddressResponse>(CommandEnum.GetAccountAddress, parameters);
            return response.Result;
        }

        public async Task<IEnumerable<string>> GetAddressesByAccount(string account)
        {
            var parameters = new JArray() { account };
            var response = await ExecuteAsync<AddressesByAccountResponse>(CommandEnum.GetAddressesByAccount, parameters);
            return response.Result;
        }

        private async Task<T> ExecuteAsync<T>(CommandEnum command, JArray parameters) where T : Response
        {
            var webRequest = (HttpWebRequest)WebRequest.Create(_bitcoindEndpoint);
            webRequest.Credentials = _bitcoindCredential;
            webRequest.ContentType = "application/json-rpc";
            webRequest.Method = "POST";

            var requestPayload = new JObject();
            requestPayload.Add(new JProperty("jsonrpc", "1.0"));
            requestPayload.Add(new JProperty("id", Guid.NewGuid()));
            requestPayload.Add(new JProperty("method", command.ToString().ToLower()));
            requestPayload.Add(new JProperty("params", parameters));

            var s = JsonConvert.SerializeObject(requestPayload);
            var bytesArray = Encoding.UTF8.GetBytes(s);
            webRequest.ContentLength = bytesArray.Length;
            using (var dataStream = webRequest.GetRequestStream())
            {
                await dataStream.WriteAsync(bytesArray, 0, bytesArray.Length);
                dataStream.Close();
            }
            var webResponse = await webRequest.GetResponseAsync();
            using (var reader = new StreamReader(webResponse.GetResponseStream()))
            {
                var response = await reader.ReadToEndAsync();
                return JsonConvert.DeserializeObject<T>(response);
            }
        }
    }
}