﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Common.BitcoindModels;
using Common.Enums;
using Common.Models;
using DAL.Repositories;

namespace WebApp.Services
{
    public class SynchronizationService
    {
        public static readonly SemaphoreSlim SyncSemaphore = new SemaphoreSlim(1, 1);
        private static IEnumerable<string> Accounts;
        private const uint GetTransactionsBatchSize = int.MaxValue;

        public async Task SynchronizeAccounts()
        {
            var service = new BitcoindProxyService();
            var accounts = await service.ListAccounts();
            if (Accounts == null)
            {
                Accounts = accounts.Select(o => o.Key);
            }
            var getAddressesTasks = new List<Task>();
            var walletsRepository = new AccountsRepository();
            var wallets = new ConcurrentBag<Account>();
            foreach (var account in accounts)
            {
                var getAddressesTask = service.GetAddressesByAccount(account.Key)
                    .ContinueWith(getAddressTask =>
                {
                    wallets.Add(new Account()
                    {
                        Name = account.Key,
                        Addresses = getAddressTask.Result,
                        Balance = account.Value
                    });
                });
                getAddressesTasks.Add(getAddressesTask);
            }
            await Task.WhenAll(getAddressesTasks);
            await walletsRepository.UpdateAccounts(wallets);
        }

        public async Task UpdateAccountsBalances()
        {
            var service = new BitcoindProxyService();
            var walletsRepository = new AccountsRepository();
            var accounts = await service.ListAccounts();
            await walletsRepository.UpdateAccountsBalances(accounts);
        }

        public async Task SynchronizeTransactions()
        {
            var bitcoindProxy = new BitcoindProxyService();
            var allGrouppedTransactions = new Dictionary<TransactionCategoryEnum, List<TransactionListItem>>();
            foreach (var account in Accounts)
            {
                var next = false;
                ulong offset = 0;
                do
                {
                    var accountTransactions = await bitcoindProxy.ListTransactions(account, GetTransactionsBatchSize, offset);
                    foreach (var transaction in accountTransactions)
                    {
                        if (transaction.Category == TransactionCategoryEnum.Send || transaction.Category == TransactionCategoryEnum.Receive)
                        {
                            allGrouppedTransactions[transaction.Category].Add(transaction);
                        }
                    }
                    offset += GetTransactionsBatchSize;
                    next = accountTransactions.Length == GetTransactionsBatchSize;
                }
                while (next);
            }
            var syncIncoming = SynchronizeIncomingTransactions(allGrouppedTransactions[TransactionCategoryEnum.Receive]);
            var syncOutgoing = SynchronizeOutgoingTransactions(allGrouppedTransactions[TransactionCategoryEnum.Send]);
            await Task.WhenAll(syncIncoming, syncOutgoing);
        }

        private async Task SynchronizeIncomingTransactions(IEnumerable<TransactionListItem> allTxs)
        {
            var txsRepository = new TransactionsRepository();
            var storedTxsIds = await txsRepository.GetIncomingTransactions();
            var txsToAdd = allTxs.Where(tx => !storedTxsIds.Contains(tx.TxId));
            var observableTxs = await txsRepository.GetObservableIncomingTransactions();
            var txsToUpdate = allTxs.Where(tx => observableTxs.Contains(tx.TxId));
            var txsIdsToRemove = storedTxsIds.Where(txId => !allTxs.Any(tx => tx.TxId == txId));
            if (txsToAdd.Any() || txsToUpdate.Any() || txsIdsToRemove.Any())
            {
                // Update balance only in case of new pr updated incoming transactions
                if (txsToAdd.Any() || txsToUpdate.Any())
                {
                    try
                    {
                        await SyncSemaphore.WaitAsync();
                        await UpdateAccountsBalances();
                    }
                    finally
                    {
                        SyncSemaphore.Release();
                    }
                }
                await txsRepository.UpdateIncomingTransactions(txsToAdd, txsToUpdate, txsIdsToRemove);
            }
        }

        private async Task SynchronizeOutgoingTransactions(IEnumerable<TransactionListItem> allTxs)
        {
            var txsRepository = new TransactionsRepository();
            var storedTxsIds = await txsRepository.GetOutgoingTransactions();
            var txsToAdd = allTxs.Where(tx => !storedTxsIds.Contains(tx.TxId));
            var txsToRemove = storedTxsIds.Where(txId => !allTxs.Any(tx => tx.TxId == txId));
            if (txsToAdd.Any() || txsToRemove.Any())
            {
                await txsRepository.UpdateOutgoingTransactions(txsToAdd, txsToRemove);
            }
        }
    }
}